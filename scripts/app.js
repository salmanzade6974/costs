class Cost {
  async X() {
    try {
      const result = await fetch("http://api-cost.vosoghi.ir/Costs");
      const data = await result.json();
      return data;
    } catch (error) {
      console.log(error);
    }
  }

  async getCosts() {
    // try {
    //   const result = await fetch("http://cost.vosoghi.ir/Costs");
    //   const data = await result.json();
    //   console.log(data);
    //   return data;
    // } catch (error) {
    //   console.log(error);
    // }

    return await axios.get("http://api-cost.vosoghi.ir/Costs").then((res) => {
      return res.data;
    });
  }

  async sendHttpRequest(method, data, url) {
    return fetch(url, {
      method: method,
      body: JSON.stringify(data),
      headers: data ? { "Content-Type": "application/json" } : {},
    }).then((res) => {
      console.log(res);
      return res.json();
    });
  }

  async postCosts(title, price, description) {
    try {
      this.sendHttpRequest(
        "POST",
        {
          Title: title,
          Price: price,
          Description: description,
        },
        "http://api-cost.vosoghi.ir/Cost/Create"
      ).then((responseData) => {
        console.log(responseData);
      });
    } catch (error) {
      console.log(error);
    }
  }

  async postBudget(price) {
    try {
      this.sendHttpRequest(
        "POST",
        {
          Price: price,
        },
        "http://api-cost.vosoghi.ir/Budget/Create"
      ).then((responseData) => {
        console.log(responseData);
      });
    } catch (error) {
      console.log(error);
    }
  }

  async budgetCreate(price) {
    console.log("Budget");
    try {
      if (price > 0) {
        this.postBudget(price);
      } else {
        alert("مقدار مبلغ الزامی است");
      }
    } catch (error) {
      console.log(error);
    }
  }

  async totalPrice() {
    try {
      let sum = 0;
      await this.X().then((data) => {
        data.Items.forEach((item) => {
          sum += item.Price;
        });
      });
      return sum;
    } catch (error) {
      console.log(error);
    }
  }

  async create(title, price, description) {
    try {
      if (title != "" && title != null && price > 0) {
        this.postCosts(title, price, description);
        alert("اطلاعات ثبت شد");
      } else {
        alert("مقادیر عنوان و مبلغ الزامی هستند");
      }
    } catch (error) {
      console.log(error);
    }
  }

  async costs() {
    try {
      let costs = [];
      await this.getCosts().then((data) => {
        costs = data;
      });
      return costs;
    } catch (error) {
      console.log(error);
    }
  }

  async costById(id) {
    try {
      const result = await fetch(`http://api-cost.vosoghi.ir/Cost/${id}`);
      const data = await result.json();
      return data;
    } catch (error) {
      console.log(error);
    }
  }

  async maxCost() {
    try {
      let costs = [];
      await this.X().then((data) => {
        costs = data.Items.sort((a, b) => {
          if (a.Price > b.Price) {
            return -1;
          } else if (a.Price < b.Price) {
            return 1;
          } else {
            return 0;
          }
        });
      });
      return costs[0];
    } catch (error) {
      console.log(error);
    }
  }

  async maxCost2() {
    try {
      let cost = [];
      await this.X().then((data) => {
        cost = data.Items[0];
        data.Items.forEach((item) => {
          if (item.Price > cost.Price) {
            cost = item;
          }
        });
      });
      return cost;
    } catch (error) {
      console.log(error);
    }
  }

  async minCost() {
    try {
      let costs = [];
      await this.X().then((data) => {
        costs = data.Items.sort((a, b) => {
          if (a.Price > b.Price) {
            return 1;
          } else if (a.Price < b.Price) {
            return -1;
          } else {
            return 0;
          }
        });
      });
      return costs[0];
    } catch (error) {
      console.log(error);
    }
  }

  async costByDate(date) {
    try {
      let costs = [];
      await this.X().then((data) => {
        costs = data.Items.filter((item) => {
          return item.Date == date;
        });
      });
      return costs;
    } catch (error) {
      console.log(error);
    }
  }

  async costByTitle(title) {
    try {
      let costs = [];
      await this.X().then((data) => {
        costs = data.Items.filter((item) => {
          return item.Title === title;
        });
      });
      return costs;
    } catch (error) {
      console.log(error);
    }
  }

  async costByGreaterThan(number) {
    try {
      let costs = [];
      await this.X().then((data) => {
        data.Items.forEach((item) => {
          if (item.Price > number) {
            costs.push(item);
          }
        });
      });
      return costs;
    } catch (error) {
      console.log(error);
    }
  }

  async maxCostByDate(date) {
    try {
      let costs = [];
      await this.X().then((data) => {
        costs = data.Items.filter((item) => {
          return item.Date == date;
        });

        costs.sort((a, b) => {
          if (a.Price > b.Price) {
            return -1;
          } else if (a.Price < b.Price) {
            return 1;
          } else {
            return 0;
          }
        });
      });
      return costs[0];
    } catch (error) {
      console.log(error);
    }
  }

  async budget() {
    try {
      let sum = 100000;
      let budget = 0;
      await this.totalPrice().then((data) => {
        sum = data;
      });
      budget = (sum * 100) / 1000000;
      return budget;
    } catch (error) {
      console.log(error);
    }
  }
}
