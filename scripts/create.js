document
  .querySelector("#add-cost-form")
  .addEventListener("submit", function (e) {
    e.preventDefault();
    const cost = new Cost();

    const title = e.target.elements.costsTitle.value.trim();
    const price = Number(e.target.elements.costsPrice.value);
    const description = e.target.elements.description.value;

    cost.create(title, price, description).then(() => {

      e.target.elements.costsTitle.value = "";
      e.target.elements.costsPrice.value = "";
      e.target.elements.description.value = "";

    })
  });
  
