const Costs = [];

const costsDOM = document.querySelector(".plan__list");
class View {
  displayCosts(costs) {
    let result = "";
    costs.Items.forEach((item) => {
      result += `
        <article class="plan">
                <div class="cost_title">
                <span>${item.Title}</span>
                <span>${item.Date}</span>
                </div>
                
                <div class="cost_price">
                <h1>${item.Price.toLocaleString()}</h1>
                </div>
                
                <div class="cost_btn">
                <a href="./detail.html#${item.Id}" class="btn">جزئیات</a>
                </div>
            </article>
        `;
    });

    costsDOM.innerHTML = result;
  }
}

document.addEventListener("DOMContentLoaded", (e) => {
  e.preventDefault()
  const view = new View();
  const cost = new Cost();
  cost.costs().then((data) => {
    view.displayCosts(data);
  })
  
});
