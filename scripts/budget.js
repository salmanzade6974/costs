document.querySelector("#add-cost-form").addEventListener("submit", (e) => {
  e.preventDefault();
  const cost = new Cost();
  const price = Number(e.target.elements.budgetPrice.value);

  cost.budgetCreate(price).then(() => {
    e.target.elements.budgetPrice.value = "";
    alert("ثبت با موفقیت انجام شد");
    location.assign("/")
  });
});
