document.addEventListener("DOMContentLoaded", (e) => {
  e.preventDefault()
  const cost = new Cost();

  cost.maxCost().then((data) => {
    document.getElementById("price_max").innerText = data.Price.toLocaleString();
  });

  cost.maxCost2().then((data) => {
    console.log(data);
  });

  cost.minCost().then((data) => {
    document.getElementById("price_min").innerText = data.Price.toLocaleString();
  });

  cost.costByDate('2021-05-31T16:36:50.4397424').then((data) => {
    console.log(data);
  });

  cost.costByTitle('خرید خواروبار').then((data) => {
    console.log(data);
  });

  cost.costByGreaterThan(6000).then((data) => {
    console.log(data);
  });

  cost.maxCostByDate('0001-01-01T00:00:00').then((data) => {
    console.log(data);
  });

  cost.totalPrice().then((data) => {
    document.getElementById("price").innerText = data.toLocaleString();
  });

  cost.budget().then((data)=> {
    console.log("Budget=: " + data);
  })
});
