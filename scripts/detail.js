document.addEventListener("DOMContentLoaded", (e) => {
  e.preventDefault()
  const cost = new Cost();
  const costId = location.hash.substring(1);
  cost.costById(costId).then((data) => {
    document.getElementById("cost_title").innerText = data.Title;
    document.getElementById("cost_price").innerText =
      data.Price.toLocaleString();
    document.getElementById("cost_date").innerText = data.Date;
    document.getElementById("cost_description").innerText = data.Description;
  });
});
